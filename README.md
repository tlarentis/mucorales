#**Analysis of the Cytochrome P450 (CYP51)in Mucormycetes**#

----
##**Strain collection**##

_Rhizopus arrhizus_ (aka _Rhizopus delemar_, _Rhizopus oryzae_)

_Rhizopus microsporus_

_Mucor circinelloides_

----
##**Sequences collection**##

**Original:**

[**Sequences for the CYP51A**](https://bitbucket.org/tlarentis/mucorales/src/b6c9c41e2b9f2758fffebb991775c9736fada0dc/CYP51A_original/?at=master)

[**Sequences for the CYP51B**](https://bitbucket.org/tlarentis/mucorales/src/b6c9c41e2b9f2758fffebb991775c9736fada0dc/CYP51B_original/?at=master)
#
**Updated:**

[**Sequences for the CYP51A**](https://bitbucket.org/tlarentis/mucorales/src/0d61b156c1d7eb43933fe08403cc23fce037fb5e/CYP51A/?at=master)

[**Sequences for the CYP51B**](https://bitbucket.org/tlarentis/mucorales/src/0d61b156c1d7eb43933fe08403cc23fce037fb5e/CYP51B/?at=master)

-----
##**17.03.2016**##

1. A basic phylogenetic overview of the sequences has been given. The original FASTA files were all put together in one file (Notepad or CAT) and aligned according to [MAFFT](http://mafft.cbrc.jp/alignment/server/ "MAFFT online alignment tool") (MAFFT is an alignment algorithm, like MUSCLE for example). In the advanced option area the option "Adjust direction according to the first sequence (accurate enough for most cases)" was choosen.

2. The alignment was copied and saved in a Notepad file, which was opened with MEGA afterwards.

3. In MEGA a phylogenetic tree was created; Neighbourjoining tree without bootstrapping.

4. The tree can be downloaded [here](https://bitbucket.org/tlarentis/mucorales/src/e1ae5a4daed82ec928ebdc2484496533e8213b94/Phylogenetic_tree_overview.pdf?fileviewer=file-view-default)